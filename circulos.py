import matplotlib.pyplot as plt

datos = []                                  #Lista auxiliar para guardar los datos del txt
odometria = []                              #Lista que almacenará todos los datos en float para graficarlos
tiempo = []
x = []
y = []
orientacion = []
vel_lineal = []
vel_ang = []

def archivo(txt):                              #Cargar datos del archivo txt
    global lista
    archivo = open(txt, 'r')     #Abrimos el txt en modo lectura.
    lista = archivo.readlines()             #Leemos las líneas y las almacenamos en una lista.
    archivo.close()                         #Cerramos el archivo.

def odom ():
    global tiempo, x, y, orientacion, vel_lineal, vel_ang
    for i in lista:             
        aux_datos = i.split('\t')               #Separamos los datos según \t.
        datos.extend(aux_datos)                 #Agrega cada dato a la lista de dato
    for i in range(len(datos)):                 #Recorremos la lista
        datos[i] = datos[i].strip()             #Eliminamos los \n
    for i in datos:
        numero_float = float(i)
        odometria.append(numero_float)
    for i in range(len(odometria)):
        lista_index = i % 6
        if lista_index == 0:
            tiempo.append(odometria[i])
        elif lista_index == 1:
            x.append(odometria[i])
        elif lista_index == 2:
            y.append(odometria[i])
        elif lista_index == 3:
            orientacion.append(odometria[i])
        elif lista_index == 4:
            vel_lineal.append(odometria[i])
        elif lista_index == 5:
            vel_ang.append(odometria[i])

def grafico_circ2 ():
    #X vs Y
    fig, ax1 = plt.subplots(figsize=(6,6))
    plt.grid(True)
    plt1 = plt.plot(x, y, color='blue', label='Camino')
    plt.annotate('', xy=(0.5, -2), xytext=(0.4, -2.03),arrowprops=dict(facecolor='red', shrink=0.04))
    plt.annotate('', xy=(0,-0.08), xytext=(0.11, -0.07),arrowprops=dict(facecolor='red', shrink=0.04))
    ax1.set_ylabel('Y')
    ax1.set_xlabel('X')
    ax1.legend()
    #Pose
    fig, ax3 = plt.subplots()
    plt.grid(True)
    ax3.set_xlim([min(tiempo), max(tiempo)])
    plt3 = plt.plot(tiempo, x, color='blue', label='X')
    plt3 = plt.plot(tiempo, y, color='red', label='Y')
    plt3 = plt.plot(tiempo, vel_ang, color='green', label='Vel_ang')
    ax3.set_ylabel('Pose')
    ax3.set_xlabel('Tiempo')
    ax3.legend()
    #vel_lineal y vel_ang vs tiempo
    fig, ax2 = plt.subplots()
    plt.grid(True)
    ax2.set_xlim([min(tiempo), max(tiempo)])
    plt2 = plt.plot(tiempo, vel_lineal, color='red', label='Vel_lin')
    plt2 = plt.plot(tiempo, vel_ang, color='yellow', label='Vel_ang')
    ax2.set_ylabel('Velocidad')
    ax2.set_xlabel('Tiempo')
    ax2.legend()
    plt.show()

def grafico_circ3 ():
    #X vs Y
    fig, ax1 = plt.subplots(figsize=(6,6))
    plt.grid(True)
    plt1 = plt.plot(x, y, color='blue', label='Camino')
    plt.annotate('', xy=(0.5, -2), xytext=(0.55, -1.96),arrowprops=dict(facecolor='red', shrink=0.04))
    plt.annotate('', xy=(0,0), xytext=(-0.11, -0.012),arrowprops=dict(facecolor='red', shrink=0.04))
    ax1.set_ylabel('Y')
    ax1.set_xlabel('X')
    ax1.legend()
    #Pose
    fig, ax3 = plt.subplots()
    plt.grid(True)
    ax3.set_xlim([min(tiempo), max(tiempo)])
    plt3 = plt.plot(tiempo, x, color='blue', label='X')
    plt3 = plt.plot(tiempo, y, color='red', label='Y')
    plt3 = plt.plot(tiempo, vel_ang, color='green', label='Vel_ang')
    ax3.set_ylabel('Pose')
    ax3.set_xlabel('Tiempo')
    ax3.legend()
    #vel_lineal y vel_ang vs tiempo
    fig, ax2 = plt.subplots()
    plt.grid(True)
    ax2.set_xlim([min(tiempo), max(tiempo)])
    plt2 = plt.plot(tiempo, vel_lineal, color='red', label='Vel_lin')
    plt2 = plt.plot(tiempo, vel_ang, color='yellow', label='Vel_ang')
    ax2.set_ylabel('Velocidad')
    ax2.set_xlabel('Tiempo')
    ax2.legend()
    plt.show()

def grafico_circ4 ():
    #X vs Y
    fig, ax1 = plt.subplots(figsize=(6,6))
    plt.grid(True)
    plt1 = plt.plot(x, y, color='blue', label='Camino')
    plt.annotate('', xy=(-1, 0.45), xytext=(-0.91, 0.34),arrowprops=dict(facecolor='red', shrink=0.04))
    plt.annotate('', xy=(0,2.25), xytext=(-0.13, 2.25),arrowprops=dict(facecolor='red', shrink=0.04))
    ax1.set_ylabel('Y')
    ax1.set_xlabel('X')
    ax1.legend()
    #Pose
    fig, ax3 = plt.subplots()
    plt.grid(True)
    ax3.set_xlim([min(tiempo), max(tiempo)])
    plt3 = plt.plot(tiempo, x, color='blue', label='X')
    plt3 = plt.plot(tiempo, y, color='red', label='Y')
    plt3 = plt.plot(tiempo, vel_ang, color='green', label='Vel_ang')
    ax3.set_ylabel('Pose')
    ax3.set_xlabel('Tiempo')
    ax3.legend()
    #vel_lineal y vel_ang vs tiempo
    fig, ax2 = plt.subplots()
    plt.grid(True)
    ax2.set_xlim([min(tiempo), max(tiempo)])
    plt2 = plt.plot(tiempo, vel_lineal, color='red', label='Vel_lin')
    plt2 = plt.plot(tiempo, vel_ang, color='yellow', label='Vel_ang')
    ax2.set_ylabel('Velocidad')
    ax2.set_xlabel('Tiempo')
    ax2.legend()

archivo ("circulo3.txt")
odom()
grafico_circ3()

